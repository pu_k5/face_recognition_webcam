import face_recognition # lib สำเร็จรูปสำหรับ Face Recognition
import os, sys
import cv2
import numpy as np
import math
# from door_control import *
from line_notification import *
import tkinter as tk
import time
import threading
import shutil

# ฟังก์ชันคำนวณเปลี่ยนเป็น % ความแม่นยำ Ex. 0.95 => 95%
def face_confidence(face_distance, face_match_threshold=0.6):
    range = (1.0 - face_match_threshold)
    linear_val = (1.0 - face_distance) / (range * 2.0)

    if face_distance > face_match_threshold:
        return str(round(linear_val * 100, 2)) + '%'
    else:
        value = (linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))) * 100
        return str(round(value, 2)) + '%'


class FaceRecognition:
    face_locations = []
    face_encodings = []
    face_names = []
    known_face_encodings = []
    known_face_names = []
    process_current_frame = True

    delay = 10
    root = tk.Tk()
    state = 0

    def __init__(self):
        self.encode_faces()

    def delayed_detection(self):
        self.state = 0

    # ฟังกชั่นนำรูปภาพจาก /face/* มาเข้ารหัสและนำชื่อภาพมาเป็นชื่อที่ใช้ในการ detect ใบหน้า
    def encode_faces(self):
        for image in os.listdir('faces'):
            face_image = face_recognition.load_image_file(f"faces/{image}")
            face_encoding = face_recognition.face_encodings(face_image)[0]
            image_name = image.split('.')[0]
            self.known_face_encodings.append(face_encoding)
            self.known_face_names.append(image_name)
        print(self.known_face_names)

    # ฟังก์ชันรันกล้องและ detect ใบหน้า
    def run_recognition(self):
        video_capture = cv2.VideoCapture(0) # เรียกวิดีโอจากกล้องตัวที่ index 0

        if not video_capture.isOpened(): # เช็คว่ามีเปิดใช้งานกล้องหรือไม่
            sys.exit('Video source not found...')

        while True:
            ret, frame = video_capture.read()

            # Only process every other frame of video to save time
            if self.process_current_frame:
                # Resize frame of video to 1/4 size for faster face recognition processing
                small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

                # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
                # เปลี่นรูปจาก RGB ที่เปิดจาก OpenCV เป็น RGB สำหรับใช้งาน lib face_recognition
                rgb_small_frame = small_frame[:, :, ::-1]

                # Find all the faces and face encodings in the current frame of video
                # ค้นหาใบหน้าและใบหน้าที่มีการเข้ารหัสไว้จาก ฟังก์ชัน encode_faces จากเฟรมปัจจุบัน
                self.face_locations = face_recognition.face_locations(rgb_small_frame)
                self.face_encodings = face_recognition.face_encodings(rgb_small_frame, self.face_locations)

                self.face_names = []
                for face_encoding in self.face_encodings:
                    # See if the face is a match for the known face(s)
                    # ค้นหาใบหน้าที่ตรงกับใบหน้าที่เข้ารหัสไว้
                    matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
                    name = "Unknown"
                    confidence = '0%'

                    # Calculate the shortest distance to face
                    # เปรียบเทียบระยะทางใบหน้ากับใบหน้าที่เข้ารหัสเพื่อหาใบหน้าที่คล้ายกัน
                    face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)

                    best_match_index = np.argmin(face_distances)
                    if matches[best_match_index]: # หากมีการพบใบหน้าที่ตรงกับที่เข้ารหัสไว้ ให้คำนวณค่าความแม่นยำ และบอกว่าเจอใครใน if นี้
                        name = self.known_face_names[best_match_index]
                        confidence = face_confidence(face_distances[best_match_index])

                        if self.state == 0: # เช็ค state เพื่อ delay
                            top, right, bottom, left = self.face_locations[-1]
                            
                            top *= 4
                            right *= 4
                            bottom *= 4
                            left *= 4

                            cropped_image = frame[left:right + 6, top:bottom + 6]
                            timestr = time.strftime("%Y%m%d%H%M%S")
                            cv2.imwrite("./images/"+timestr+".jpg",cropped_image)
                            SendNotification(name, timestr)
                            self.state = 1
                            # SendDoorOpen()
                            timer = threading.Timer(self.delay, self.delayed_detection)
                            timer.start()

                        print(name)

                    self.face_names.append(f'{name} ({confidence})')

            self.process_current_frame = not self.process_current_frame

            # Display the results
            # แสดงผลลัพย์ ตีกรอบสีเพื่อแสดงตำแหน่งใบหน้าในวิดีโอ
            for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
                # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                # Create the frame with the name
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                # cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                cv2.putText(frame, name, (left + 6, bottom - 6), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1)

            # Display the resulting image
            # แสดงผลลัพย์ที่ได้
            cv2.imshow('Face Recognition', frame)

            # Hit 'q' on the keyboard to quit!
            # กด q เพื่อออกจากหน้าแสดงวิดีโอ
            if cv2.waitKey(1) == ord('q'):
                break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    print("Face Recognition Running...")
    fr = FaceRecognition()
    fr.run_recognition()
