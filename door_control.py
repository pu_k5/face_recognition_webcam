from gpiozero import LED
from time import sleep

door = LED(17)

def SendDoorOpen() :
    door.on()
    sleep(3)
    door.off()